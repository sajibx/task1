package com.example.demo.Data

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class employees(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Long? = null,
    var name:String? = null
)

@Entity
class files(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Long? = null,
    var name:String? = null,
    var type:String? = null
)

@Entity
class resullt(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val employees_id: Long? = null,
    var file_type:String? = null,
    var reason:String? = null
)